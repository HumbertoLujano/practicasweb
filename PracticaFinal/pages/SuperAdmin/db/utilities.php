<?php

require_once('conexion.php'); 

//Funcion que permite agregar, un registro o modificarlo
function add_cat($nombre, $descripcion){
    global $pdo;
    $current_date_time = date('Y-m-d H:i:s');
    $sql = "INSERT INTO categoria (nombre_categoria, descripcion_categoria, fecha_categoria) VALUES ('$nombre',' $descripcion', '$current_date_time')";
    $statement = $pdo->prepare($sql);

    $statement->execute();
}


// Función para obtener todas las categorías de la base de datos
function getAllCategorias(){
    global $pdo;
    $sql = "SELECT * from categoria";
    $statement = $pdo->prepare($sql);
    $statement->execute();
    $results=$statement->fetchAll();
    return $results;
}


//Funcion que permite eliminar la informacion de un registro
function deletecat($id){
    global $pdo;
    try {
    $sql = "DELETE FROM categoria where id_categoria='$id'";
    $statement = $pdo->prepare($sql);
    $statement->execute();
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

}

    //Funcion que permite realizar una busqueda de los datos 
    function searchCAT($id){
        global $pdo;

        $sql = "SELECT * FROM categoria where id_categoria='$id'";
        $statement = $pdo->prepare($sql);
        $statement->execute();
        $results=$statement->fetchAll();

        return $results[0];
    }

        //Funcion que permite realizar una busqueda de los datos 
        function searchINV($id){
            global $pdo;
    
            $sql = "SELECT * FROM inventario where id_inventario='$id'";
            $statement = $pdo->prepare($sql);
            $statement->execute();
            $results=$statement->fetchAll();
    
            return $results[0];
        }

        	//Funcion que permite actualizar algun registro existente dependiendo de su  id
    function updateINV($id,$codigo_inventario,$nombre_producto){
        global $pdo;
        $current_date_time = date('Y-m-d H:i:s');
        $sql = "UPDATE inventario SET codigo_inventario='$codigo_inventario', nombre_producto='$nombre_producto',precioProducto_inventario='$precioProducto_inventario', stock = '$stock' WHERE id_inventario='$id'";
        $statement = $pdo->prepare($sql);
        $statement->execute();
        $results=$statement->fetchAll();

        return $results[0];
    }


	//Funcion que permite actualizar algun registro existente dependiendo de su  id
    function updateCAT($id,$nombre,$descripcion){
        global $pdo;
        $current_date_time = date('Y-m-d H:i:s');
        $sql = "UPDATE categoria SET nombre_categoria='$nombre', descripcion_categoria='$descripcion',fecha_categoria='$current_date_time' WHERE id_categoria='$id'";
        $statement = $pdo->prepare($sql);
        $statement->execute();
        $results=$statement->fetchAll();

        return $results[0];
    }

    	//Funcion que permite obtener todos los registros en otra tabla.
	    function fill_sel(){
		global $pdo;

		$sql = "SELECT id_categoria, descripcion_categoria FROM categoria";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results;
	}


    // Función para obtener todos los productos de la base de datos

function getAllProductos(){
    global $pdo;
    $sql = "SELECT * from inventario";
    $statement = $pdo->prepare($sql);
    $statement->execute();
    $results=$statement->fetchAll();
    return $results;
}


//Funcion que permite eliminar la informacion de un registro
function deleteproduc($id){
    global $pdo;
    try {
    $sql = "DELETE FROM inventario where id_inventario='$id'";
    $statement = $pdo->prepare($sql);
    $statement->execute();
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

}

//Funcion que permite agregar, un registro o modificarlo
function add_producto($codigo_inventario, $nombre_producto, $precioProducto_inventario, $id_categoria_producto, $stock){
    global $pdo;
    $current_date_time = date('Y-m-d H:i:s');
    $sql = "INSERT INTO inventario (codigo_inventario,nombre_producto,fechaA_inventario,precioProducto_inventario,id_categoria_producto,stock) VALUES('$codigo_inventario', '$nombre_producto',  '$current_date_time', '$precioProducto_inventario','$id_categoria_producto', '$stock')";
    $statement = $pdo->prepare($sql);
    $statement->execute();
}





?>