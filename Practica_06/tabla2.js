var datos = [
	{
		id: 1,
		nombres: "Juan Carlos",
		apellidoPaterno: "García",
		apellidoMaterno: "Hernández",
		email: "juancarlos@gmail.com",
		telefono: "555-123-4567",
		direccion: "Calle 1 #123, Col. Centro, Ciudad de México",
		rfc: "GAHJ900101",
		categoria: "Provedor"
	},
	{
		id: 2,
		nombres: "María Fernanda",
		apellidoPaterno: "López",
		apellidoMaterno: "Martínez",
		email: "maferlm@gmail.com",
		telefono: "555-987-6543",
		direccion: "Av. Reforma #456, Col. Juárez, Ciudad de México",
		rfc: "LOMM920202",
		categoria: "Proveedor"
	},
	{
		id: 3,
		nombres: "Pedro Antonio",
		apellidoPaterno: "Pérez",
		apellidoMaterno: "González",
		email: "pedropz@gmail.com",
		telefono: "555-555-5555",
		direccion: "Calle 2 #456, Col. Roma, Ciudad de México",
		rfc: "PEGJ800101",
		categoria: "Provedor"
	},
	{
		id: 4,
		nombres: "Ana",
		apellidoPaterno: "González",
		apellidoMaterno: "Martínez",
		email: "anagonz@gmail.com",
		telefono: "555-111-1111",
		direccion: "Calle 3 #456, Col. Del Valle, Ciudad de México",
		rfc: "GOMA780101",
		categoria: "Provedor"
	},
	{
		id: 5,
		nombres: "Jorge",
		apellidoPaterno: "Hernández",
		apellidoMaterno: "Rojas",
		email: "jorgehr@gmail.com",
		telefono: "555-222-2222",
		direccion: "Calle 4 #456, Col. Narvarte, Ciudad de México",
		rfc: "HERJ850101",
		categoria: "Proveedor"
	},
	{
		id: 6,
		nombres: "Sofía",
		apellidoPaterno: "García",
		apellidoMaterno: "Sánchez",
		email: "sofiags@gmail.com",
		telefono: "555-333-3333",
		direccion: "Calle 5 #456, Col. Condesa, Ciudad de México",
		rfc: "GASO950101",
		categoria: "Provedor"
	},
	{
		id: 7,
		nombres: "Diego",
		apellidoPaterno: "Torres",
		apellidoMaterno: "Hernández",
		email: "diegotorres@gmail.com",
		telefono: "555-444-4444",
		direccion: "Calle 6 #456, Col. Roma Norte, Ciudad de México",
		rfc: "TOHD920101",
		categoria: "Provedor"
	},
	{
		id: 8,
		nombres: "Karla",
		apellidoPaterno: "Fernández",
		apellidoMaterno: "García",
		email: "karlafg@gmail.com",
		telefono: "555-555-5555",
		direccion: "Calle 7 #456, Col. Anzures, Ciudad de México",
		rfc: "FEGK900101",
		categoria: "Proveedor"
	},
	{
		id: 9,
		nombres: "José Luis",
		apellidoPaterno: "Gutiérrez",
		apellidoMaterno: "Hernández",
		email: "joselgh@gmail.com",
		telefono: "555-666-6666",
		direccion: "Calle 8 #456, Col. San Ángel, Ciudad de México",
		rfc: "GUHJ830101",
		categoria: "Provedor"
	},
	{
		id: 10,
		nombres: "Fernanda",
		apellidoPaterno: "Lara",
		apellidoMaterno: "Pérez",
		email: "ferlara@gmail.com",
		telefono: "555-777-7777",
		direccion: "Calle 9 #456, Col. Polanco, Ciudad de México",
		rfc: "LAPF960101",
		categoria: "Provedor"
	}
];
var tablaDatos = document.getElementById("tablaDatos");

for (var i = 0; i < datos.length; i++) {
	var fila = document.createElement("tr");

	var celdaId = document.createElement("td");
	celdaId.appendChild(document.createTextNode(datos[i].id));
	fila.appendChild(celdaId);

	var celdaNombres = document.createElement("td");
	celdaNombres.appendChild(document.createTextNode(datos[i].nombres));
	fila.appendChild(celdaNombres);

	var celdaApellidoPaterno = document.createElement("td");
	celdaApellidoPaterno.appendChild(document.createTextNode(datos[i].apellidoPaterno));
	fila.appendChild(celdaApellidoPaterno);

	var celdaApellidoMaterno = document.createElement("td");
	celdaApellidoMaterno.appendChild(document.createTextNode(datos[i].apellidoMaterno));
	fila.appendChild(celdaApellidoMaterno);

	var celdaEmail = document.createElement("td");
	celdaEmail.appendChild(document.createTextNode(datos[i].email));
	fila.appendChild(celdaEmail);

	var celdaTelefono = document.createElement("td");
	celdaTelefono.appendChild(document.createTextNode(datos[i].telefono));
	fila.appendChild(celdaTelefono);

	var celdaDireccion = document.createElement("td");
	celdaDireccion.appendChild(document.createTextNode(datos[i].direccion));
	fila.appendChild(celdaDireccion);

	var celdaRfc = document.createElement("td");
	celdaRfc.appendChild(document.createTextNode(datos[i].rfc));
	fila.appendChild(celdaRfc);

	var celdaCategoria = document.createElement("td");
	celdaCategoria.appendChild(document.createTextNode(datos[i].categoria));
	fila.appendChild(celdaCategoria);

	tablaDatos.appendChild(fila);
}
