

const menu = [
  {
    id: 1,
    title: "MONSTER TRUCK (FAST & FURIOUS) ",
    category: "MONSTER",
    price: 15.99,
    img: "./imagenes/FsatMonster.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	Fast & Furious
    Escala	1:64 `,
  },
  {
    id: 2,
    title: " MONSTER TRUCK (VENOM) ",
    category: "MONSTER",
    price: 13.99,
    img: "./imagenes/venomMonster.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	VENOM
    Escala	1:64 `,
  },
  {
    id: 3,
    title: "MONSTER TRUCK (BOB ESPONJA) ",
    category: "MONSTER",
    price: 6.99,
    img: "./imagenes/bobMonster.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	BOB ESPONJA
    Escala	1:64`,
  },
  {
    id: 4,
    title: "MONSTER TRUCK (DODGE CHARGER R/T) ",
    category: "MONSTER",
    price: 20.99,
    img: "./imagenes/monster01.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	DODGE CHARGER
    Escala	1:64 `,
  },
  {
    id: 5,
    title: "MONSTER TRUCK (SKELETOR) ",
    category: "MONSTER",
    price: 22.99,
    img: "./imagenes/monster02.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	SKELETOR
    Escala	1:64 `,
  },
  {
    id: 6,
    title: "MONSTER TRUCK (THANOS)",
    category: "MONSTER",
    price: 18.99,
    img: "./imagenes/tanosMonster.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	MARVEL
    Escala	1:64`,
  },
  {
    id: 7,
    title: "MONSTER TRUCK (SPIDER-MAN) ",
    category: "MONSTER",
    price: 8.99,
    img: "./imagenes/spiderMonster.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	spider-man
    Escala	1:64 `,
  },
  {
    id: 8,
    title: "DONUT DRIFTER",
    category: "FUNY",
    price: 232.09,
    img: "./imagenes/dona.jpeg",
    desc: `......  `,
  },
  {
    id: 9,
    title: "MONSTER TRUCK TURTLES (LEONARDO)",
    category: "MONSTER",
    price: 365.030,
    img: "./imagenes/tortugaMonster.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Monster Trucks
    Modelo del vehículo	Tortugas Ninja
    Escala	1:64`,
  },
  {
    id: 10,
    title: "SNOOPY",
    category: "FUNY",
    price: 22.99,
    img: "./imagenes/snupy.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Hot Wheels
    Modelo del vehículo	Snoopy
    Escala	1:64`,
  },


  {
    id: 11,
    title: "SPEED STRIKER",
    category: "MOTOS",
    price: 15.99,
    img: "./imagenes/moto01.jpeg", 
    desc: `Fabricante	Mattel
    Marca	Matchbox
    Marca del vehículo	Speed
    Modelo del vehículo	Striker
    Escala	1:64 `,
  },
  {
    id: 12,
    title: "HONDA SUPER CUB",
    category: "MOTOS",
    price: 13.99,
    img: "./imagenes/moto02.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Honda
    Modelo del vehículo	Super Cub
    Escala	1:64 `,
  },
  {
    id: 13,
    title: "HW450",
    category: "MOTOS",
    price: 6.99,
    img: "./imagenes/moto03.jpeg",
    desc: `Dirt Bike a escala 1:64. Color amarillo con manillar azul. 3/5 en la serie HW Daredevils 2018 `,
  },
  {
    id: 14,
    title: "BMW RnineT RACER",
    category: "MOTOS",
    price: 129.00,
    img: "./imagenes/moto04.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Hot Wheels
    Modelo del vehículo	BMW R NINET RACER
    Escala	1:64 `,
  },
  {
    id: 15,
    title: "KOOL KOMBI",
    category: "COMBI",
    price: 22.99,
    img: "./imagenes/combi.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Volkswagen
    Modelo del vehículo	Kool Combi
    Versión del vehículo	The Beatles
    Escala	1:64`,
  },
  {
    id: 16,
    title: "KOOL KOMBI",
    category: "COMBI",
    price: 18.99,
    img: "./imagenes/combi02.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Volkswagen
    Modelo del vehículo	Kool Combi
    Versión del vehículo	The Beatles
    Escala	1:64`,
  },
  {
    id: 17,
    title: "KOOL KOMBI ",
    category: "COMBI",
    price: 8.99,
    img: "./imagenes/combi03.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Volkswagen
    Modelo del vehículo	Kool Combi
    Versión del vehículo	The Beatles
    Escala	1:64 `,
  },
  {
    id: 18,
    title: "KOOL KOMBI",
    category: "COMBI",
    price: 12.99,
    img: "./imagenes/combi04.jpeg",
    desc: ` Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Volkswagen
    Modelo del vehículo	Kool Combi
    Versión del vehículo	The Beatles
    Escala	1:64 `,
  },
  {
    id: 19,
    title: "KOOL KOMBI",
    category: "COMBI",
    price: 16.99,
    img: "./imagenes/combi05.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Volkswagen
    Modelo del vehículo	Kool Combi
    Versión del vehículo	The Beatles
    Escala	1:64`,
  },
  {
    id: 20,
    title: "HOT WHEELS HIGH",
    category: "CAMION",
    price: 22.99,
    img: "./imagenes/camion.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Hot Wheels
    Modelo del vehículo	Hot Wheels High
    Escala	1:64`,
  },



  {
    id: 21,
    title: "HOT WHEELS HIGH",
    category: "CAMION",
    price: 15.99,
    img: "./imagenes/camion02.jpeg", 
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Hot Wheels
    Modelo del vehículo	Hot Wheels High
    Escala	1:64 `,
  },
  {
    id: 22,
    title: "HOT WHEELS HIGH",
    category: "CAMION",
    price: 13.99,
    img: "./imagenes/camion04.jpeg",
    desc: `Fabricante	Mattel
    Marca	Hot Wheels
    Marca del vehículo	Hot Wheels
    Modelo del vehículo	Hot Wheels High
    Escala	1:64 `,
  },
  {
    id: 23,
    title: "70 DODGE CHARGER",
    category: "MOVIE",
    price: 45.00,
    img: "./imagenes/movie.jpeg",
    desc: `Cargador Dodge '70 Hot Wheels Baja Blazers Fast & Furious`,
  },
  {
    id: 24,
    title: "BATMOBILE",
    category: "MOVIE",
    price: 70.00,
    img: "./imagenes/movie02.jpeg",
    desc: `Batimóvil Batman V Superman Retro Entertainment VS 1:64 Hot Wheels DWJ91 DMC55 (Hot Wheels Batman v Superman) `,
  },
  {
    id: 25,
    title: "BACK TO THE FUTURE TIME MACHINE",
    category: "MOVIE",
    price: 50.00,
    img: "./imagenes/movie03.jpeg",
    desc: `DeLorean Regreso al Futuro Time Machine Modelo 1:64 Hot Wheels Retro DJF49 (Hot Wheels Retro Entertainment Diecast Back To The Future Time Machine Vehicle) `,
  },
];



// Obtener elemento principal
const sectionCenter = document.querySelector(".section-center");
const btnContainer = document.querySelector(".btn-container");


// mostrar todos los productos al cargar la página
//NOTA: "DOMContentLoaded" permite conocer el momento en el que todos los elementos del DOM, es decir, los elementos HTML de un proyecto, están cargados
window.addEventListener("DOMContentLoaded", function () {
  diplayMenuItems(menu);
  displayMenuButtons();
});

//Items de productos
function diplayMenuItems(menuItems) {
  let displayMenu = menuItems.map(function (item) {
    // console.log(item);

    return `<article class="menu-item">
          <img src=${item.img} alt=${item.title} class="photo" />
          <div class="item-info">
            <header>
              <h4>${item.title}</h4>
              <h4 class="price">$${item.price}</h4>
            </header>
            <p class="item-text">
              ${item.desc}
            </p>
          </div>
        </article>`;
  });
  displayMenu = displayMenu.join("");
  // console.log(displayMenu);

  sectionCenter.innerHTML = displayMenu;
}

//Opciones de botones
function displayMenuButtons() {
  const categories = menu.reduce(
    function (values, item) {
      if (!values.includes(item.category)) {
        values.push(item.category);
      }
      return values;
    },
    ["all"]
  );
  //constructor del boton con el nombre de la categoría de los items del array
  const categoryBtns = categories
    .map(function (category) {
      return `<button type="button" class="filter-btn" data-id=${category}>
          ${category}
        </button>`;
    })
    .join("");

  btnContainer.innerHTML = categoryBtns;
  const filterBtns = btnContainer.querySelectorAll(".filter-btn");
  console.log(filterBtns);

  filterBtns.forEach(function (btn) {
    btn.addEventListener("click", function (e) {
      // console.log(e.currentTarget.dataset);
      const category = e.currentTarget.dataset.id;
      const menuCategory = menu.filter(function (menuItem) {
        // console.log(menuItem.category);
        if (menuItem.category === category) {
          return menuItem;
        }
      });
      if (category === "all") {
        diplayMenuItems(menu);
      } else {
        diplayMenuItems(menuCategory);
      }
    });
  });
}
