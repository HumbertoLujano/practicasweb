<?php

include_once('db/database_utilities.php');

$t = $_GET["t"];

$user_access = getAll($t);           //Se obtienen todos los registros y se llena el array mediante los usuarios encontrados en la base de datos.
$total_users = count($user_access); //Se hace un conteo de cuantos registros se tinen en el sistema.
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 06 </title>
    <link rel="stylesheet" href="./styles/bootstrap.min.css" />
  </head>
  <body class="d-flex flex-column min-vh-100">
    
    



    <div class="card m-5" data-section>
    <div class="card-header d-flex flex-wrap justify-content-between align-items-center">
    <h4>Listado de productos<h4> <a href="add.php?t=<?php echo($t) ?>" class="btn btn-primary ">Agregar nuevo registro</a>
    </div >
    <section class="section">
      <div class="card-body " data-slug="panel1">
    
        
          <div class="table-responsive ">
          <?php if($total_users){ ?>
          <table class="table table-hover ">
            <thead>
              <tr>
                <th>Id</th>
                <th>Descripcion</th>
                <th>Precio de venta</th>
                <th>Precio de compra</th>
                <th>Categoria</th>
              </tr>
            </thead>
            <tbody class="table-group-divider ">
            <?php foreach( $user_access as $id => $user ){ ?>

              <tr>
                <td class="align-middle"> <?php echo $user['id'] ?></td>
                <td class="align-middle"><?php echo $user['descripcion'] ?></td>
                <td class="align-middle"><?php echo $user['precio_venta'] ?></td>
                <td class="align-middle"><?php echo $user['precio_compra'] ?></td>
                <td class="align-middle"><?php echo $user['nombre_categoria'] ?></td>
                <td class="align-middle"><a href="update.php?id=<?php echo($user['id']); ?>&t=<?php echo($t) ?>"  class="btn btn-warning btn-block btn-sm" >Modificar</a></td>
                <td class="align-middle"><a href="delete.php?id=<?php echo($user['id']); ?>" class="btn btn-danger btn-sm" onClick="wait();">Eliminar</a></td>
              </tr>
              <?php } ?>

              <tr>
              <td colspan="4"  class="align-middle" ><b>Total de registros: </b> <?php echo $total_users; ?></td>
              </tr>
            </tbody>
          </table>
              <?php }else{ ?>
              No hay registros
              <?php } ?>
        </div>
      </div>
    </section>
  </div>
    <script type="text/javascript">
        //Funcion que permite cancelar el evento en caso de querer borrar un archivo.
        function wait(){
          var r = confirm("¿Desea eliminar el registro?");
          if (!r) 
              event.preventDefault();
        }
    </script>

    </body>


</html>









