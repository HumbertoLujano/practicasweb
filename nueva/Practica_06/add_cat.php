<?php
include_once('db/database_utilities.php');

$t=$_GET['t'];
//Se revisa que las variables se esten recibiendo para continuar con la insercion de los valores ingresados 
if(isset($_POST['descripcion'])){
  add_cat($_POST['descripcion']);
  header("location: listado_cat.php?t=".$_GET['t']."");
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 06 </title>
    <link rel="stylesheet" href="./styles/bootstrap.min.css" />

  </head>
  <body class="d-flex flex-column min-vh-100">
    


    <div class="card m-5" data-section>
    <div class="card-header d-flex flex-wrap justify-content-between align-items-center">
    <h4>Agregar nueva categoria<h4> 
    </div >
    <section class="section">
      <div class="card-body " data-slug="panel1">
      <form method="POST" action="add_cat.php?t=<?php echo($t)?>">
        <div class="row g-3">
          

          <div class="mb-2 col-6">
            <label class="form-label" for="descripcion">descripcion:</label>
            <input type="text" class="form-control" name="descripcion">
          </div>

          <button type="submit" class="btn btn-primary">Registrar</button>
        </div>
      </form>
      </div>
    </section>
  </div>

  
