
<header class="p-3 text-bg-dark">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="index.php"><img src="./media/upvlogo.png"  width="" height="50" >   </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto ms-4 justify-content-center mb-md-0">

          <li><a href="./index.php" class="a-bg btn btn-secondary btn-lg px-4 mx-2 btn-sm">Inicio </a></li>

          <li><a href="./listado.php?t=2" class="a-bg btn btn-secondary btn-lg px-4 mx-2 btn-sm">Productos </a></li>

          <li><a href="./listado_cat.php?t=1" class="a-bg btn btn-secondary btn-lg px-4 me-sm-3 mx-2 btn-sm">Categorias</a></li>

        </ul>

      </div>
    </div>
  </header>