<?php
include_once('db/database_utilities.php');

$id = isset( $_GET['id'] ) ? $_GET['id'] : '';  
$r = searchCAT($id); //Se realiza una busqueda en la base de datos 


//Se revisa que la variable se encuentre definida
if(isset($_POST['descripcion'])){

  //Se realiza la actualizacion del registro 
  updateCAT($id,$_POST['descripcion']);

  //Al termino de la actualizacion se redirige a la pagina anterior en el listado
  header("location: listado_cat.php?t=".$_GET['t']);
  
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 06 </title>
    <link rel="stylesheet" href="./styles/bootstrap.min.css" />
  </head>
  <body class="d-flex flex-column min-vh-100">
    

    <div class="card m-5" data-section>
        <div class="card-header d-flex flex-wrap justify-content-between align-items-center">
            <h4>Modificar producto<h4> 
        </div >
        <section class="section">
            <div class="card-body " data-slug="panel1">
                <?php echo('<form method="POST" action="update_cat.php?id='.$id.'&t='.$_GET['t'].'">');?>
                    <div class="row g-3">
                        <div class="mb-2 col-6">
                            <label class="form-label" for="id">Id:</label>
                            <input type="text" class="form-control" name="id" value="<?php echo($r['id'])?>" disabled>
                        </div>

                        <div class="mb-2 col-6">
                            <label class="form-label" for="descripcion">Nombre:</label>
                            <input type="text" class="form-control" name="descripcion" value="<?php echo($r['descripcion'])?>">
                        </div>

                        <button type="submit" class="btn btn-success" onClick="wait();">Modificar</button>

                    </div>
                </form>
            </div>
        </section>
    </div>
     
   <script type="text/javascript">
        //Funcion que permite cancelar el evento en caso de querer modificar un registro.
        function wait(){
          var r = confirm("¿Desea modificar el registro?");
          if (!r) 
              event.preventDefault();
        }
    </script>


