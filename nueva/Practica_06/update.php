<?php
include_once('db/database_utilities.php');


$t = $_GET["t"];

//Se obtienen todos los registros 
$user_access = fill_sel();           


$id = isset( $_GET['id'] ) ? $_GET['id'] : '';  //Se revisa que el id se encuentre mediante el metodo get.
$r = search($id); //Se realiza una busqueda en la base de datos donde se obtienen los atributos del registro con el id ingresado.


if(isset($_POST['descripcion']) && isset($_POST['precio_venta'])){
  update($id,$_POST['descripcion'],$_POST['precio_venta'],$_POST['precio_compra'],$_POST['id_categoria_producto']);
  header("location: listado.php?t=".$_GET['t']);
  
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 06 </title>
    <link rel="stylesheet" href="./styles/bootstrap.min.css" />
  </head>
  <body class="d-flex flex-column min-vh-100">
    

    <div class="card m-5" data-section>
        <div class="card-header d-flex flex-wrap justify-content-between align-items-center">
            <h4>Modificar producto<h4> 
        </div >
        <section class="section">
            <div class="card-body " data-slug="panel1">
                <?php
                  echo('<form method="POST" action="update.php?id='.$id.'&t='.$_GET['t'].'">');
                ?>
                    <div class="row g-3">
                        <div class="mb-2 col-6">
                            <label class="form-label" for="id">Id:</label>
                            <input type="text" class="form-control" name="id" value="<?php echo($r['id'])?>" disabled>
                        </div>

                        <div class="mb-2 col-6">
                            <label class="form-label" for="descripcion">Nombre:</label>
                            <input type="text" class="form-control" name="descripcion" value="<?php echo($r['descripcion'])?>">
                        </div>

                        <div class="mb-2 col-6">
                            <label class="form-label" for="precio_venta">precio venta:</label>
                            <input type="text" class="form-control" name="precio_venta" value="<?php echo($r['precio_venta'])?>">
                        </div>
                        
                        <div class="mb-2 col-6">
                            <label class="form-label" for="precio_compra">precio compra:</label>
                            <input type="text" class="form-control" name="precio_compra" value="<?php echo($r['precio_compra'])?>">
                        </div>

                        <div class="mb-2 col-6">
                            <label class="form-label" for="id_categoria_producto">precio de compra:</label>
                            <?php 
                                echo '<select name="id_categoria_producto" class="form-select">';
                                foreach ($user_access as $row) {
                                echo '<option value="' . $row['id'] . '">' . $row['descripcion'] . '</option>';
                                }
                                echo '</select>';
                            ?>
                        </div>

                        <button type="submit" class="btn btn-success" onClick="wait();">Modificar</button>

                    </div>
                </form>
            </div>
        </section>
    </div>




     
   <script type="text/javascript">
        //Funcion que permite cancelar el evento en caso de querer modificar un registro.
        function wait(){
          var r = confirm("¿Desea modificar el registro?");
          if (!r) 
              event.preventDefault();
        }
    </script>


