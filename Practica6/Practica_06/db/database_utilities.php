<?php
	include('connection.php');

	//Funcion que cuenta los registros en la base de datos
	function count_status(){
		global $pdo;
		$sql = "SELECT COUNT(*) as total_status FROM status";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();
		return $results[0]['total_status'];
	}

	//Funcion que permite obtener todos los registros en otra tabla.
	function selectAllFromTable($table){
		global $pdo;

		$sql = "SELECT * FROM $table";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results;
	}

	//Funcion que permite obtener todos los registros en otra tabla.
	function fill_sel(){
		global $pdo;

		$sql = "SELECT id, descripcion FROM categorias";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results;
	}


	//Funcion que permite obtener todos los uaurios de la tabla productos
	function getAll(){
		global $pdo;

		$sql = "SELECT p.id, p.descripcion, p.precio_venta, p.precio_compra, c.descripcion AS nombre_categoria
		FROM productos AS p
		JOIN categorias AS c ON p.id_categoria_producto = c.id";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results;
	}

	//Funcion que permite obtener todos los uaurios de la tabla categorias
	function getAllCat(){
		global $pdo;

		$sql = "SELECT * from categorias";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results;
	}


	//Funcion que permite agregar, un registro o modificarlo
	function add($descripcion,$precio_venta, $precio_compra, $id_categoria_producto){
		global $pdo;
		$sql = "INSERT INTO productos (descripcion,precio_venta,precio_compra,id_categoria_producto) VALUES('$descripcion','$precio_venta','$precio_compra','$id_categoria_producto')";
		$statement = $pdo->prepare($sql);
		$statement->execute();
	}

	//Funcion que permite agregar, un registro o modificarlo
	function add_cat($descripcion){
		global $pdo;

		$sql = "INSERT INTO categorias (descripcion) VALUES('$descripcion')";
		$statement = $pdo->prepare($sql);

		$statement->execute();
	}

	//Funcion que permite realizar una bosqueda de los datos de uno de los registros
	function search($id){
		global $pdo;

		$sql = "SELECT * FROM productos where id='$id'";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results[0];
	}

	//Funcion que permite realizar una busqueda de los datos 
	function searchCAT($id){
		global $pdo;

		$sql = "SELECT * FROM categorias where id='$id'";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results[0];
	}

	//Funcion que permite actualizar algun registro existente dependiendo de su id
	function update($id,$descripcion,$precio_venta, $precio_compra, $id_categoria_producto){
		global $pdo;

		$sql = "UPDATE productos SET descripcion='$descripcion', precio_venta='$precio_venta', precio_compra='$precio_compra', id_categoria_producto='$id_categoria_producto' WHERE id='$id'";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		$results=$statement->fetchAll();

		return $results[0];
	}

		//Funcion que permite actualizar algun registro existente dependiendo de su  id
		function updateCAT($id,$descripcion){
			global $pdo;
	
			$sql = "UPDATE categorias SET descripcion='$descripcion' WHERE id='$id'";
			$statement = $pdo->prepare($sql);
			$statement->execute();
			$results=$statement->fetchAll();
	
			return $results[0];
		}

	//Funcion que permite eliminar la informacion de un registro
	function delete($id){
		global $pdo;
		
		$sql = "DELETE FROM productos where id='$id'";
		$statement = $pdo->prepare($sql);

		$statement->execute();
	}

	//Funcion que permite eliminar la informacion de un registro
	function deletecat($id){
		global $pdo;
		try {
		$sql = "DELETE FROM categorias where id='$id'";
		$statement = $pdo->prepare($sql);
		$statement->execute();
		} catch (Exception $e) {
			echo 'Excepción capturada: ',  $e->getMessage(), "\n";
		}

	}
?>

