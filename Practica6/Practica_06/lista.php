<?//php

//include_once('db/database_utilities.php');

//$t = $_GET["t"];

//$user_access = getAll($t);           //Se obtienen todos los registros y se llena el array mediante los usuarios encontrados en la base de datos.
//$total_users = count($user_access); //Se hace un conteo de cuantos registros se tinen en el sistema.
?>
<!doctype html>
<html lang="en" data-bs-theme="auto">
  <head><script src="../assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.111.3">
    <title>Lista</title>
   <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

   <div class="button">
<form method="GET" action="index.php">

  <button class="btn btn-warning" class="button" type="submit">Principal</button>
</form>
</div>

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"> Gestión de Productos</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
    
        </div>
      </div>

      <h2>Productos</h2>
      <div class="table-responsive">
        <td><a href="./add.php?t=<?//php echo($t) ?>" class="btn btn-info">Agregar nuevo registro</a></td>
        <?//php if($total_users){ ?>
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">ID </th>
              <th scope="col">Descripción</th>
              <th scope="col">Precio de Venta</th>
              <th scope="col">Precio de Compra</th>
              <th scope="col">ID Categoría Producto</th>
            </tr>
          </thead>
          <tbody>
          <?//php foreach( $user_access as $id => $user ){ ?>
                  <tr>
                    <td><?//php echo $user['id'] ?></td>
                    <td><?//php echo $user['descripcion'] ?></td>
                    <td><?//php echo $user['precio_Venta'] ?></td>
                    <td><?//php echo $user['precio_Compra'] ?></td>
                    <td><?//php echo $user['id_productos'] ?></td>
                    <?//Se generan dos botones, que redireccionan a acutalizaar y eliminar respectivamente."?>
                    <td><a href="./update.php?id=<?//php echo($user['id']); ?>&t=<?//php echo($t) ?>" class="button radius tiny warning"">Modificar</a></td>
                    <td><a href="./delete.php?id=<?//php echo($user['id']); ?>" class="button radius tiny alert" onClick="wait();">Eliminar</a></td>
                  </tr>
                  <?//php } ?>
                  <tr>
                    <td colspan="4"><b>Total de registros: </b> <?//php echo $total_users; ?></td>
                  </tr>
          </tbody>
        </table>
        <?php //}else{ ?>
              No hay registros
              <?//php } ?>
      </div>
    </main>
  </div>
</div>









    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

      <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/chart.js@4.2.1/dist/chart.umd.min.js" integrity="sha384-gdQErvCNWvHQZj6XZM0dNsAoY4v+j5P1XDpNkcM3HJG1Yx04ecqIHk7+4VBOCHOG" crossorigin="anonymous"></script><script src="dashboard.js"></script>

     <!---- <script type="text/javascript">
        //Funcion que permite cancelar el evento en caso de querer borrar un archivo.
        function wait(){
          var r = confirm("¿Desea eliminar el usuario?");
          if (!r) 
              event.preventDefault();
        }
    </script>--> 

    <?//php require_once('footer.php'); ?>

  </body>
</html>
