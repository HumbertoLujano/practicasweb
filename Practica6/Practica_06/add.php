<?php
include_once('db/database_utilities.php');




$t = $_GET["t"];

$user_access = fill_sel();     //Se obtienen todos los registros y se llena el array.


//Se revisa que las variables se esten recibiendo con la insercion de los valores ingresados en la base de datos
if(isset($_POST['descripcion']) && isset($_POST['precio_venta']) && isset($_POST['precio_compra'])&& isset($_POST['id_categoria_producto'])){
  add($_POST['descripcion'],$_POST['precio_venta'],$_POST['precio_compra'],$_POST['id_categoria_producto'],$t);
  header("location: listado.php?t=".$_GET['t']."");
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
  <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 06 </title>
    <script src="../assets/js/color-modes.js"></script>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/buttons/">
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">

  </head>
  <body class="d-flex flex-column min-vh-100">
    


    <div class="card m-5" data-section>
    <div class="card-header d-flex flex-wrap justify-content-between align-items-center">
    <h4>Agregar nuevo producto<h4> 
    </div >
    <section class="contenedordos">
      <div class="card-body " data-slug="panel1">
      <form method="POST" action="add.php?t=<?php echo($t)?>">
        <div class="row g-3">
          

          <div class="mb-2 col-6">
            <label class="form-label" for="descripcion">descripcion:</label>
            <input type="text" class="form-control" name="descripcion">
          </div>

          <div class="mb-2 col-6">
            <label class="form-label" for="precioventa">precio de venta:</label>
            <input type="text" class="form-control" name="precio_venta">
          </div>
          
          <div class="mb-2 col-6">
            <label class="form-label" for="preciocompra">precio de compra:</label>
            <input type="text" class="form-control" name="precio_compra">
          </div>

          <div class="mb-2 col-6">
            <label class="form-label" for="id_categoria_producto">categoria:</label>
              <?php 
                echo '<select name="id_categoria_producto" class="form-select">';
                  foreach ($user_access as $row) {
                  echo '<option value="' . $row['id'] . '">' . $row['descripcion'] . '</option>';
                }
                echo '</select>';
              ?>
          </div>

          <button type="submit" class="btn btn-success">Registrar</button>
        </div>
      </form>
      </div>
    </section>
  </div>
  <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

  
