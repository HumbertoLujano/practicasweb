<?php

include_once('db/database_utilities.php');

$t = $_GET["t"];

$user_access = getAllCat();           //Se obtienen todos los registros y se llena el array mediante los usuarios encontrados en la base de datos.
$total_users = count($user_access); //Se hace un conteo de cuantos registros se tinen en el sistema.
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Practica 06 </title>
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body class="d-flex flex-column min-vh-100">
    
   <div class="button">
<form method="GET" action="index.php">

  <button class="btn btn-warning" class="button" type="submit">Principal</button>
</form>
</div>
    



    <div class="card m-5" data-section>
    <div class="card-header d-flex flex-wrap justify-content-between align-items-center">
    <h4>Listado de categorias<h4> <a href="./add_cat.php?t=<?php echo($t) ?>" class="btn btn-primary ">Agregar nuevo registro</a>
    </div >
    <section class="section">
      <div class="card-body " data-slug="panel1">
    
        
          <div class="table-responsive ">
          <?php if($total_users){ ?>
          <table class="table table-hover ">
            <thead>
              <tr>
                <th>Id</th>
                <th>Descripcion</th>
              </tr>
            </thead>
            <tbody class="table-group-divider ">
            <?php foreach( $user_access as $id => $user ){ ?>

              <tr>
                <td class="align-middle"> <?php echo $user['id'] ?></td>
                <td class="align-middle"><?php echo $user['descripcion'] ?></td>
                <td class="align-middle"><a href="./update_cat.php?id=<?php echo($user['id']); ?>&t=<?php echo($t) ?>"  class="btn btn-warning btn-block btn-sm" >Modificar</a></td>
                <td class="align-middle"><a href="./delete_cat.php?id=<?php echo($user['id']); ?>" class="btn btn-danger btn-sm" onClick="wait();">Eliminar</a></td>
              </tr>
              <?php } ?>

              <tr>
              <td colspan="4"  class="align-middle" ><b>Total de registros: </b> <?php echo $total_users; ?></td>
              </tr>
            </tbody>
          </table>
              <?php }else{ ?>
              No hay registros
              <?php } ?>
        </div>
      </div>
    </section>
  </div>

  <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/chart.js@4.2.1/dist/chart.umd.min.js" integrity="sha384-gdQErvCNWvHQZj6XZM0dNsAoY4v+j5P1XDpNkcM3HJG1Yx04ecqIHk7+4VBOCHOG" crossorigin="anonymous"></script><script src="dashboard.js"></script>
    <script type="text/javascript">
        //Funcion que permite cancelar el evento en caso de querer borrar un archivo.
        function wait(){
          var r = confirm("¿Desea eliminar el registro?");
          if (!r) 
              event.preventDefault();
        }
    </script>

    </body>


</html>




