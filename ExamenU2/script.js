
/**La función mostrarModal() recupera los valores de cuatro campos de entrada diferentes por sus respectivos IDs, 
 * los almacena en variables y luego los asigna al innerHTML de elementos con IDs correspondientes en una ventana modal. Finalmente, 
 * establece la propiedad de visualización CSS del modal en "block", lo que lo hace visible en la página. */
function mostrarModal() {
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var email = document.getElementById("email").value;
    var tttr=document.getElementById("ttt").value;
    document.getElementById("nombreModal").innerHTML = nombre;
    document.getElementById("apellidoModal").innerHTML = apellido;
    document.getElementById("emailModal").innerHTML = email;
    document.getElementById("tttModal").innerHTML = tttr;
    document.getElementById("miModal").style.display = "block";
  }
  /**La función anónima asignada a la variable cerrar se ejecuta cuando se hace clic en el elemento "cerrar". 
   * Establece la propiedad de visualización CSS del modal en "none", lo que lo hace invisible en la página. */
  var cerrar = document.getElementsByClassName("cerrar")[0];
  cerrar.onclick = function() {
    document.getElementById("miModal").style.display = "none";
  }