/**El código es una lista de objetos que contiene información sobre los diferentes productos y los clientes que los compraron. 
 * La información incluida en cada objeto es el usuario, el nombre del cliente, el producto, una descripción del producto, el precio y la fecha de entrega. */

const pedidos = [
    { usuario: "0", nombre: "juan", producto: "Hot Wheels id Smart Track Upgrade Kit", descripcion: "Descripción del producto 1", precio: 100 ,fechaentrega: "6 de Marzo"},
    { usuario: "1", nombre: "Sofía", producto: "Nissan Skyline GT-R (BNR34)", descripcion: "Descripción del producto 2", precio: 2045, fechaentrega: "18 de Mayo"},
    { usuario: "2", nombre: "Luciana ", producto: "'70 Ford Escort RS1600", descripcion: "Descripción del producto 1", precio: 103, fechaentrega: "23 de Julio"},
    { usuario: "34", nombre: "Federico ", producto: "67 Pontiac Firebird 400", descripcion: "Descripción del producto 2", precio: 210,fechaentrega: "9 de Septiembre" },
    { usuario: "6", nombre: "Juan Pablo ", producto: "Lamborghini Veneno", descripcion: "Descripción del producto 1", precio: 710, fechaentrega: "13 de Octubreo"},
    { usuario: "9", nombre: "Camila ", producto: "Volkswagen T1 Panel Bus", descripcion: "Descripción del producto 2", precio: 220, fechaentrega: "22 de Noviembre"},
    { usuario: "5", nombre: "Tomás ", producto: "15 Mercedes-AMG GT", descripcion: "Descripción del producto 1", precio: 160, fechaentrega: "1 de Diciembre"},
    { usuario: "12", nombre: "Victoria ", producto: "Porsche 911 GT3 RS", descripcion: "Descripción del producto 2", precio: 920, fechaentrega: "17 de Enero"},
    { usuario: "78", nombre: "Valentina ", producto: "Mazda RX-7 FD", descripcion: "Descripción del producto 1", precio: 610, fechaentrega: "8 de Febreroo" },
    { usuario: "56", nombre: "Martín ", producto: "Tesla Model S", descripcion: "Descripción del producto 2", precio: 420,fechaentrega: "28 de Marzo" },
    // Agregar aquí los otros 10 pedidos
  ];
  /**luego crea una tabla en HTML con un ID de 'tabla-pedidos' y utiliza un bucle 'for' para iterar a través de todos los objetos de la lista 'pedidos'.
   *  Para cada objeto, se crean nuevas filas y celdas de tabla HTML y se agrega la información correspondiente a cada celda. Luego, se agrega cada fila completa a la tabla HTML. */
  const tablaPedidos = document.getElementById("tabla-pedidos");
  
  for (let i = 0; i < pedidos.length; i++) {
    const pedido = pedidos[i];
  
    const fila = document.createElement("tr");
    const celdaUsuario = document.createElement("td");
    const celdaNombre = document.createElement("td");
    const celdaProducto = document.createElement("td");
    const celdaDescripcion = document.createElement("td");
    const celdaPrecio = document.createElement("td");
    const celdafechaentrega=document.createElement("td");
  
    celdaUsuario.textContent = pedido.usuario;
    celdaNombre.textContent = pedido.nombre;
    celdaProducto.textContent = pedido.producto;
    celdaDescripcion.textContent = pedido.descripcion;
    celdaPrecio.textContent = `$${pedido.precio}`;
    celdafechaentrega.textContent=pedido.fechaentrega;
  
    fila.appendChild(celdaUsuario);
    fila.appendChild(celdaNombre);
    fila.appendChild(celdaProducto);
    fila.appendChild(celdaDescripcion);
    fila.appendChild(celdaPrecio);
    fila.appendChild(celdafechaentrega);
  
    tablaPedidos.appendChild(fila);
  }
  