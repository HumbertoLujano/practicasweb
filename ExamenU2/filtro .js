
/**definiendo un array llamado 'menu' que contiene objetos 
 * que representan productos. Cada objeto tiene un 'id', 'title', 'category', 'price', 'img', y 'desc'. */
const menu = [

    {
      id: 3,
      title: "MONSTER TRUCK (BOB ESPONJA) ",
      category: "MONSTER",
      price: 6.99,
      img: "./img/bobMonster.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Monster Trucks
      Modelo del vehículo	BOB ESPONJA
      Escala	1:64`,
    },
    {
      id: 4,
      title: "MONSTER TRUCK (DODGE CHARGER R/T) ",
      category: "MONSTER",
      price: 20.99,
      img: "./img/monster01.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Monster Trucks
      Modelo del vehículo	DODGE CHARGER
      Escala	1:64 `,
    },
    {
      id: 5,
      title: "MONSTER TRUCK (SKELETOR) ",
      category: "MONSTER",
      price: 22.99,
      img: "./img/monster02.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Monster Trucks
      Modelo del vehículo	SKELETOR
      Escala	1:64 `,
    },
 
    {
      id: 7,
      title: "MONSTER TRUCK (SPIDER-MAN) ",
      category: "MONSTER",
      price: 8.99,
      img: "./img/spiderMonster.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Monster Trucks
      Modelo del vehículo	spider-man
      Escala	1:64 `,
    },
    {
      id: 8,
      title: "DONUT DRIFTER",
      category: "FUNY",
      price: 232.09,
      img: "./img/dona.jpeg",
      desc: `......  `,
    },

    {
      id: 10,
      title: "SNOOPY",
      category: "FUNY",
      price: 22.99,
      img: "./img/snupy.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Hot Wheels
      Modelo del vehículo	Snoopy
      Escala	1:64`,
    },

    {
      id: 11,
      title: "SPEED STRIKER",
      category: "MOTOS",
      price: 15.99,
      img: "./img/moto01.jpeg", 
      desc: `Fabricante	Mattel
      Marca	Matchbox
      Marca del vehículo	Speed
      Modelo del vehículo	Striker
      Escala	1:64 `,
    },
    {
      id: 12,
      title: "HONDA SUPER CUB",
      category: "MOTOS",
      price: 13.99,
      img: "./img/moto02.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Honda
      Modelo del vehículo	Super Cub
      Escala	1:64 `,
    },
    {
      id: 13,
      title: "HW450",
      category: "MOTOS",
      price: 6.99,
      img: "./img/moto03.jpeg",
      desc: `Dirt Bike a escala 1:64. Color amarillo con manillar azul. 3/5 en la serie HW Daredevils 2018 `,
    },
    {
      id: 14,
      title: "BMW RnineT RACER",
      category: "MOTOS",
      price: 129.00,
      img: "./img/moto04.jpeg",
      desc: `Fabricante	Mattel
      Marca	Hot Wheels
      Marca del vehículo	Hot Wheels
      Modelo del vehículo	BMW R NINET RACER
      Escala	1:64 `,
    },
  ];
  
  
  
  // Obtener elemento principal
  const sectionCenter = document.querySelector(".section-center");
  const btnContainer = document.querySelector(".btn-container");
  
  /**Luego, el código define dos funciones principales:
 'displayMenuItems': esta función toma un array de objetos de menú y los convierte en código HTML que 
 se muestra en la página.
 'displayMenuButtons': esta función crea botones dinámicamente para cada categoría de producto en el 
 array 'menu'. Cuando se hace clic en un botón, se filtran los productos que pertenecen a esa categoría. */
  
  // mostrar todos los productos al cargar la página
  //NOTA: "DOMContentLoaded" permite conocer el momento en el que todos los elementos del DOM, es decir, los elementos HTML de un proyecto, están cargados
  window.addEventListener("DOMContentLoaded", function () {
    diplayMenuItems(menu);
    displayMenuButtons();
  });
  
  //Items de productos
  function diplayMenuItems(menuItems) {
    let displayMenu = menuItems.map(function (item) {
      // console.log(item);
  
      return `<article class="menu-item">
            <img src=${item.img} alt=${item.title} class="photo" />
            <div class="item-info">
              <header>
                <h4>${item.title}</h4>
                <h4 class="price">$${item.price}</h4>
              </header>
              <p class="item-text">
                ${item.desc}
              </p>
            </div>
          </article>`;
    });
    displayMenu = displayMenu.join("");
    // console.log(displayMenu);
  /**El código también define dos constantes:
   * 'sectionCenter': esta constante es el contenedor principal donde se mostrarán los productos. */
    sectionCenter.innerHTML = displayMenu;
  }
  
  //Opciones de botones
  function displayMenuButtons() {
    const categories = menu.reduce(
      function (values, item) {
        if (!values.includes(item.category)) {
          values.push(item.category);
        }
        return values;
      },
      ["all"]
    );
    //constructor del boton con el nombre de la categoría de los items del array
    //** 'btnContainer': esta constante es el contenedor para los botones de categoría. */
    const categoryBtns = categories
      .map(function (category) {
        return `<button type="button" class="filter-btn" data-id=${category}>
            ${category}
          </button>`;
      })
      .join("");
  
    btnContainer.innerHTML = categoryBtns;
    const filterBtns = btnContainer.querySelectorAll(".filter-btn");
    console.log(filterBtns);
  
    filterBtns.forEach(function (btn) {
      btn.addEventListener("click", function (e) {
        // console.log(e.currentTarget.dataset);
        const category = e.currentTarget.dataset.id;
        const menuCategory = menu.filter(function (menuItem) {
          // console.log(menuItem.category);
          if (menuItem.category === category) {
            return menuItem;
          }
        });
        if (category === "all") {
          /**Finalmente, el código agrega un event listener 
           * que llama a 'displayMenuItems' y 'displayMenuButtons' cuando la página se carga por completo. */
          diplayMenuItems(menu);
        } else {
          diplayMenuItems(menuCategory);
        }
      });
    });
  }
  
